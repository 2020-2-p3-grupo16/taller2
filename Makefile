all: ejecutable

ejecutable: main.o mostrarBytes.o
	gcc -Wall -fsanitize=address,undefined main.o mostrarBytes.o -I cabecera.h -o ejecutable
main.o: main.c
	gcc -c -Wall -fsanitize=address,undefined main.c -o main.o
mostrarBytes.o: mostrarBytes.c
	gcc -c -Wall -fsanitize=address,undefined mostrarBytes.c -o mostrarBytes.o
clean:
	rm -f ejecutable *.o
